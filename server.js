require('dotenv')
require('./src/Config/database')
const { authRoute, userRoute } = require('./src/routes')
const express = require('express')
const cors = require('cors')

const app = express()

app.use(express.json({ extended: false }))
app.use(cors())
/**
 * test
 */
app.get('/', (req, res) => {
  res.send('SERVER')
})

/**
 * Routes
 */
app.use(authRoute)
app.use(userRoute)

/**
 * SERVER
 */
const { PORT } = process.env

app.listen(PORT, () => {
  console.log(`process runing on port: ${PORT}`)
})
