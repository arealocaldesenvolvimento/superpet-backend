const userRoute = require('./UserRoutes')
const authRoute = require('./AuthRoutes')

module.exports = { userRoute, authRoute }
