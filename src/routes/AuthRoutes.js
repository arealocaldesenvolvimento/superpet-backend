const express = require('express')
const routes = express.Router()

const auth = require('../middlewares/auth')
const validationFieldResult = require('../middlewares/validationFieldsResult')

const { body } = require('express-validator')

const AuthController = require('../Controllers/AuthController')
const { Router } = require('express')

routes.get('/auth', auth, AuthController.refreshToken)

routes.post(
  '/auth',
  [
    body('email', 'Email é obrigatório!').notEmpty(),
    body('password', 'A senha é obrigatória!').notEmpty(),
    validationFieldResult
  ],
  AuthController.login
)

module.exports = routes
