const express = require('express')
const routes = express.Router()

const auth = require('../middlewares/auth')

const validationFieldsResult = require('../middlewares/validationFieldsResult')
const { body } = require('express-validator')
const UserController = require('../Controllers/UserController')

routes.get('/users', auth, UserController.index)

routes.get('/users/:id', UserController.show)

routes.post(
  '/users',
  [
    body('name', 'Nome é obrigatório!').notEmpty(),
    body('email', 'Email é obrigatório!').notEmpty(),
    body('phone', 'Telefone é obrigatório!').notEmpty(),
    validationFieldsResult
  ],
  UserController.store
)

routes.put(
  '/users/:id',
  [
    body('name', 'Nome é obrigatório!').notEmpty(),
    body('email', 'E-mail é obrigatório!').notEmpty(),
    body('phone', 'Telefone é obrigatório!').notEmpty(),
    validationFieldsResult
  ],
  auth,
  UserController.update
)

routes.delete('/users/:id', auth, UserController.delete)

module.exports = routes
