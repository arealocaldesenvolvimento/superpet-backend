require('dotenv').config()
const User = require('../Models/User')
const { Unauthorized, BadRequest } = require('../Config/genericErrors')
const { compare } = require('../Config/encrypt')
const { generateToken } = require('../Config/jwt')

exports.refreshToken = async (req, res, next) => {
  try {
    const { _id: userId } = req.user
    const user = await User.findById(userId)

    generateToken({ payload: { ...user }, expiresIn: 2 * 3600 }, (err, token) => {
      if (err) {
        throw new Unauthorized('Requisição não autorizada!')
      }

      return res.json({ token, user: user })
    })
  } catch (error) {
    next(error)
  }
}

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body

    let user = await User.findOne({ email })
    if (!user) {
      throw new BadRequest('Email ou senha incorretos!')
    }

    const isValid = await compare(password, user.password)
    if (!isValid) {
      throw new BadRequest('E-mail ou senha incorretos!')
    }

    generateToken({ payload: { ...user }, expiresIn: 2 * 3600 }, (err, token) => {
      if (err) {
        throw new Unauthorized('Requisição não autorizada!')
      }

      return res.json({ token, user })
    })
  } catch (error) {
    next(error)
  }
}
