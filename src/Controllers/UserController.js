const User = require('../Models/User')
const { BadRequest } = require('../Config/genericErrors')
const { encrypt } = require('../Config/encrypt')

exports.index = async (_, res, next) => {
  try {
    const users = await User.find({}).lean()

    return res.json(users)
  } catch (error) {
    next(error)
  }
}

exports.show = async (req, res, next) => {
  try {
    const { id } = req.params
    const user = await User.findById(id)

    return res.json(user)
  } catch (error) {
    next(error)
  }
}

exports.store = async (req, res, next) => {
  const { name, email, password, phone, company, company_phone, cpf, birth } = req.body
  try {
    const invalidUser = await User.findOne({ email })
    if (invalidUser) {
      throw new BadRequest('Este email já está em uso.')
    }

    let encryptedPassword
    if (password) {
      encryptedPassword = await encrypt(password)
    }

    const user = new User({
      name,
      email,
      password: password ? encryptedPassword : '',
      phone,
      company,
      company_phone,
      cpf : cpf ? cpf : '',
      birth : birth ? birth : ''
    })

    const newUser = await User.create(user)

    return res.json(newUser)
  } catch (error) {
    next(error)
  }
}

exports.update = async (req, res, next) => {
  try {
    const { id } = req.params
    const { name, phone, company, company_phone } = req.body

    const user = await User.findOneAndUpdate(
      { _id: id },
      { $set: { name, phone, company, company_phone } },
      { new: true }
    )

    return res.json(user)
  } catch (error) {
    next(error)
  }
}

exports.delete = async (req, res, next) => {
  try {
    const { id } = req.params
    const user = await User.findByIdAndDelete(id)

    return res.json(user)
  } catch (error) {
    next(error)
  }
}
