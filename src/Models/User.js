const { Schema, model } = require('mongoose')

const UserSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      select: true
    },
    cpf: {
      type: String
    },
    birth: {
      type: Date
    },
    phone: {
      type: String,
      required: true
    },
    company: {
      type: String
    },
    company_phone: {
      type: String
    }
  },
  {
    timestamps: true,
    versionKey: false
  }
)

UserSchema.methods.toJSON = function () {
  var obj = this.toObject()
  delete obj.password
  return obj
}

module.exports = model('User', UserSchema)
