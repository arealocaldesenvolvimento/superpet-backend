const { validationResult } = require('express-validator')

const { BadRequest } = require('../Config/genericErrors')

module.exports = validationFieldsResult = async (req, _, next) => {
  try {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      throw new BadRequest(errors.array()[0].msg)
    }

    next()
  } catch (err) {
    next(err)
  }
}
