const { isValidToken } = require('../Config/jwt')

const { Unauthorized } = require('../Config/genericErrors')

const auth = async (req, _, next) => {
  try {
    let token = req.header('Authorization')

    if (!token) {
      throw new Unauthorized('Requisição não autorizada!')
    }

    token = token.toString().replace('Bearer ', '')

    isValidToken({ token }, (err, decode) => {
      if (err) {
        throw new Unauthorized('Requisição não autorizada!')
      }

      req.user = decode._doc
      req.token = token

      next()
    })
  } catch (error) {
    next(error)
  }
}

module.exports = auth
