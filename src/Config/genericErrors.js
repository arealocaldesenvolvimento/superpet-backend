class GenericError extends Error {
  constructor(message) {
    super()
    this.message = message
  }

  getCode() {
    if (this instanceof BadRequest) {
      return 422
    }
    if (this instanceof NotFound) {
      return 404
    }
    if (this instanceof Unauthorized) {
      return 401
    }
    return 500
  }
}

class BadRequest extends GenericError {}
class NotFound extends GenericError {}
class Unauthorized extends GenericError {}

module.exports = {
  GenericError,
  Unauthorized,
  BadRequest,
  NotFound
}
