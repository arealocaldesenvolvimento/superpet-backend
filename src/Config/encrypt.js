const bcrypt = require('bcryptjs')

exports.encrypt = async (string) => {
  const salt = await bcrypt.genSalt(10)
  const encrypted = await bcrypt.hash(string, salt)

  return encrypted
}

exports.compare = async (value, compareValue) => {
  const isEqual = await bcrypt.compare(value, compareValue)

  return isEqual
}
