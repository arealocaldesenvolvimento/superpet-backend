require('dotenv').config()

const jwt = require('jsonwebtoken')

exports.generateToken = async ({ payload, expiresIn }, callback) => {
  const { SECRET_APPLICATION } = process.env

  const token = jwt.sign(payload, SECRET_APPLICATION, { expiresIn }, (err, encoded) => {
    if (err) {
      return callback(err, null)
    }
    return callback(null, encoded)
  })
}

exports.isValidToken = ({ token }, callback) => {
  const { SECRET_APPLICATION } = process.env

  jwt.verify(token, SECRET_APPLICATION, (err, decoded) => {
    if (err) {
      return callback(err, null)
    }

    return callback(null, decoded)
  })
}
