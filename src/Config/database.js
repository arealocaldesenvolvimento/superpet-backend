require('dotenv').config()

const { connect } = require('mongoose')

const { MONGODB_HOST, MONGODB_NAME } = process.env

const connection = connect(MONGODB_HOST, {
  dbName: MONGODB_NAME,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: true,
  useUnifiedTopology: true
})
  .then(() => {
    console.log('connected')
  })
  .catch((err) => {
    console.log(err.message)
    process.exit(1)
  })

module.exports = connection
